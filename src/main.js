const { BrowserWindow, ipcMain, shell, app } = require('electron')
const os = require("os");
const fs = require("fs");
const path = require("path");

let reportWindow = undefined;
const Product = require('./models/Product')
const Sale = require('./models/Sale');

function createWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 700,
        webPreferences: {
            nodeIntegration: true
        },
        title: 'Sistema de invetario'
    });
    win.setMenuBarVisibility(false);
    win.loadFile('src/ui/index.html');
    win.on("closed", () => {
        app.quit();
    });

    reportWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true
        }
    });
    reportWindow.loadFile("src/ui/report.html");
    reportWindow.hide();
    // reportWindow.webContents.openDevTools();
}

ipcMain.on('get-products', async (e, args) => {
    const products = await Product.find();
    e.reply('products-obtained', JSON.stringify(products));
});

ipcMain.on('create-product', async (e, args) => {
    const newProduct =  new Product(args);
    const productSaved = await newProduct.save();
    e.reply('product-created', JSON.stringify(productSaved));
});

ipcMain.on('delete-product', async (e, args) => {
    const productDeleted = await Product.findByIdAndDelete(args);
    e.reply('product-deleted', JSON.stringify(productDeleted));
});

ipcMain.on('update-product', async (e, args) => {
    const updatedProduct =  await Product.findByIdAndUpdate(
        args.idProductToUpdate, {
            provider: args.provider,
            brand: args.brand,
            code: args.code,
            color: args.color,
            quantity: args.quantity,
            size: args.size,
            cost: args.cost,
            price: args.price
        }, { new: true });
        e.reply('product-updated', JSON.stringify(updatedProduct));
});

ipcMain.on('get-sales', async (e, args) => {
    const sales = await Sale.find();
    e.reply('sales-obtained', JSON.stringify(sales));
});

ipcMain.on('register-sale', async (e, args) => {
    const newSale =  new Sale(args);
    newSale.date = new Date();
    const saleSaved = await newSale.save();
    e.reply('sale-created', JSON.stringify(saleSaved));
});

ipcMain.on("printPDF", (event, content) => {
    reportWindow.webContents.send("printPDF", content);
});

ipcMain.on("readyToPrintPDF", (event) => {
    reportWindow.webContents.print({});
});

module.exports = { createWindow }
