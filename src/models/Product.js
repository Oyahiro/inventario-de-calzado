const { model, Schema } = require('mongoose')

const newProductSchema = new Schema({
    provider: { type: String, required: true},
    brand: { type: String, required: true },
    code: { type: String, required: true},
    color: { type: String, required: true},
    quantity: { type: Number, required: true},
    size: { type: String, required: true },
    cost: { type: Number, required: true},
    price: { type: Number, required: true}
});

module.exports = model('Product', newProductSchema);
