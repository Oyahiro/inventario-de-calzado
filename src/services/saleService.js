const saleForm = document.querySelector('#saleForm')
const navSaleTab = document.querySelector('#nav-sale-tab')

let numberOfBadgeNotifications = 0;

function addToSellList(sale, productsToSell, invQuantity) {
    const existingProduct = productsToSell.find(p => p.product_id === sale.product_id);
    let add = true;

    if(existingProduct) {
        productsToSell = productsToSell.map(p => {
            if(p.product_id === existingProduct.product_id) {
                if(p.quantity+1 <= invQuantity){
                    p.quantity = p.quantity + 1;
                    let total = parseFloat(p.price * p.quantity).toFixed(2);
                    p.total = parseFloat(total);
                } else {
                    add = false;
                    console.log('Ya no quedan');
                }
            }
            return p;
        });
    } else {
        if(invQuantity>0){
            sale.total = parseFloat(sale.total);
            productsToSell.push(sale);
        } else {
            add = false;
            console.log('Ya no quedan');
        }
    }
    if(add) {
        addToBadge();
    }
    $("#collapseSale").collapse('show');
    return productsToSell;
}

function editToSellList(product_id, new_price, productsToSell) {
    productsToSell.map(p => {
        if(p.product_id === product_id) {
            p.price = parseFloat(new_price);
            let total = parseFloat(p.price * p.quantity).toFixed(2);
            p.total = parseFloat(total);
        }
        return p;
    });
    return productsToSell;
}

function removeToSellList(product_id, productsToSell) {
    let index = -1;
    productsToSell = productsToSell.map(p => {
        if(p.product_id === product_id) {
            p.quantity = p.quantity - 1;
            let total = parseFloat(p.price * p.quantity).toFixed(2);
            p.total = parseFloat(total);
            if(p.quantity === 0) {
                index = productsToSell.indexOf(p);
            }
        }
        return p;
    });
    if(index !== -1) 
        productsToSell.splice(index, 1);
    return productsToSell;
}

function addToBadge() {
    numberOfBadgeNotifications +=1;
    if(numberOfBadgeNotifications === 0) {
        navSaleTab.innerHTML += `<span class="badge badge-pill badge-info">${numberOfBadgeNotifications}</span>`;
    } else {
        navSaleTab.innerHTML = `Venta <span class="badge badge-pill badge-info">${numberOfBadgeNotifications}</span>`;
    }
}

function cleanNotifications() {
    numberOfBadgeNotifications = 0;
    navSaleTab.innerHTML = 'Venta';
}

function setDefaultUserData() {
    saleUserName.value = 'Consumidor final';
    saleCI.value = '0000000000';
    saleAddress.value = 'S/N';
}

function cleanUserDataToSale() {
    saleUserName.value = '';
    saleCI.value = '';
    saleAddress.value = '';
}

function cleanAllSale() {
    saleForm.reset();
    $("#collapseSale").collapse('hide');
    return [];
}

module.exports = {
    "addToSellList": addToSellList,
    "editToSellList": editToSellList,
    "removeToSellList": removeToSellList,
    "addToBadge": addToBadge,
    "cleanNotifications": cleanNotifications,
    "setDefaultUserData": setDefaultUserData,
    "cleanUserDataToSale": cleanUserDataToSale,
    "cleanAllSale": cleanAllSale
}