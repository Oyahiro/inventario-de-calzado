const reportDailyDate = document.querySelector('#reportDailyDate')
const reportMonthlyDate = document.querySelector('#reportMonthlyDate')
const providerCanvas = document.querySelector('#providerCanvas')
const brandCanvas = document.querySelector('#brandCanvas')

let months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

function generateDailyReport(sales, type) {
    let day;
    if(type === 'today'){ 
        day = new Date().toDateString();
    } else if(type === 'preview') {
        let UTCdate = new Date(reportDailyDate.value).toUTCString();
        if(UTCdate === 'Invalid Date')
            return undefined;
        else{
            day = UTCdate[0]+UTCdate[1]+UTCdate[2]+' '+UTCdate[8]+UTCdate[9]+UTCdate[10]+' '+UTCdate[5]+UTCdate[6]+' '+UTCdate[12]+UTCdate[13]+UTCdate[14]+UTCdate[15];
        }
    }
    let filteredSales = sales.filter(s => {
        return s.date === day;
    });
    let HTMLContent = generateHTMLContent(filteredSales, day);
    return HTMLContent;
}

function generateMonthlyReport(sales, type) {
    let month;
    let year;
    if(type === 'actual'){ 
        let date = new Date();
        month = date.getMonth()+1;
        year = date.getFullYear();
    } else if(type === 'preview') {
        let date = new Date(reportMonthlyDate.value);
        if(date === 'Invalid Date')
            return undefined;
        else{
            month = date.getUTCMonth()+1;
            year = date.getUTCFullYear();
        }
    }
    let filteredSales = sales.filter(s => {
        saleDate = new Date(s.date);
        saleMonth = saleDate.getUTCMonth()+1;
        saleYear = saleDate.getFullYear();
        return saleMonth === month && saleYear === year;
    });

    let HTMLContent = generateHTMLContentForMonth(filteredSales, month, year);
    return HTMLContent;
}

function loadCharts(sales) {
    let providerLabels = [];
    let providerData = [];
    let providerColors = [];
    let brandLabels = [];
    let brandData = [];
    let brandColors = [];

    sales.map(s => {
        let index = -1;
        if(!providerLabels.includes(s.provider)){
            providerLabels.push(s.provider);
            index = providerLabels.indexOf(s.provider);
            providerColors[index] = getRandomColor();
            providerData[index] = 0;
        } else {
            index = providerLabels.indexOf(s.provider);
        }
            
        providerData[index] += (s.price * s.quantity);        

        let index2 = -1;
        if(!brandLabels.includes(s.brand)){
            brandLabels.push(s.brand);
            index2 = brandLabels.indexOf(s.brand);
            brandColors[index2] = getRandomColor();
            brandData[index2] = 0;
        } else {
            index2 = brandLabels.indexOf(s.brand);
        }
        brandData[index2] += (s.price * s.quantity);
    });

    providerData = providerData.map(d => {
        return parseFloat(d.toFixed(2));
    });

    brandData = brandData.map(d => {
        return parseFloat(d.toFixed(2));
    });

    new Chart(providerCanvas, {
        type: "bar",
        data: {
            labels: providerLabels,
            datasets:[{
                label: "Ganancias",
                backgroundColor: providerColors,
                data: providerData
            }]
        }
    });

    new Chart(brandCanvas, {
        type: "bar",
        data: {
            labels: brandLabels,
            datasets:[{
                label: "Ganancias",
                backgroundColor: brandColors,
                data: brandData
            }]
        }
    });
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function generateHTMLContent(sales, date) {
    let ventas = 0;
    let ganancias = 0;
    let articulos = 0;
    let content = `<h2>Reporte de ventas</h2>`;
    content += `<h4>Fecha: `+ date +`</h4><br>`;
    content += `<table class="table table-borderless table-hover table-sm">
            <thead>
                <tr class="table-success">
                    <th scope="col"><h5>Cantidad</h5></th>
                    <th scope="col"><h5>Marca</h5></th>
                    <th scope="col"><h5>Talla</h5></th>
                    <th scope="col"><h5>Color</h5></th>
                    <th scope="col"><h5>Código</h5></th>
                    <th scope="col"><h5>Valor</h5></th>
                    <th scope="col"><h5>Total</h5></th>
                </tr>
            </thead>
            <tbody>`;

    sales.map(s => {
        content += `
            <tr>
                <td>${s.quantity}</td>
                <td>${s.brand}</td>
                <td>${s.size}</td>
                <td>${s.color}</td>
                <td>${s.code}</td>
                <td>${s.price}</td>
                <td>${s.total}</td>
            </tr>`;
            articulos += s.quantity;
            ventas += s.total;
            ganancias += s.cost * s.quantity;
    });
    ganancias = ventas - ganancias;
    content+=`
            </tbody>
        </table>
        <br>
        <div style="position: absolute;">
            <table class="table table-bordered" style="text-align: center;">
                <thead>
                    <tr>
                        <th colspan="2" scope="col">Resumen</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Articulos vendidos</b></td>
                        <td>${articulos}</td>
                    </tr>
                    <tr>
                        <td><b>Valor en ventas</b></td>
                        <td>$${parseFloat(ventas).toFixed(2)}</td>
                    </tr>
                    <tr>
                        <td><b>Valor de ganancias</b></td>
                        <td>$${parseFloat(ganancias).toFixed(2)}</td>
                    </tr>
                </tbody>
            </table>
        </div>`;

    
    return content;
}

function generateHTMLContentForMonth(sales, month, year) {
    sales.reverse();
    let ventas = 0;
    let ganancias = 0;
    let articulos = 0;
    let daySales = null;
    let content = `<h2>Reporte de ventas</h2>`;
    content += `<h4>Fecha: `+ months[month-1] + ` ` + year +`</h4>`;

    let headerTable = `
        <table class="table table-borderless table-hover table-sm">
            <thead>
                <tr class="table-success">
                    <th scope="col"><h5>Cantidad</h5></th>
                    <th scope="col"><h5>Marca</h5></th>
                    <th scope="col"><h5>Talla</h5></th>
                    <th scope="col"><h5>Color</h5></th>
                    <th scope="col"><h5>Código</h5></th>
                    <th scope="col"><h5>Valor</h5></th>
                    <th scope="col"><h5>Total</h5></th>
                </tr>
            </thead>
            <tbody>`;
    let endTable = `
            </tbody>
        </table>`;
    

    sales.map(s => {
        if(s.date !== daySales){
            if(daySales !== null)
                content += endTable;
            daySales = s.date;
            content += `Dia: ` + new Date(s.date).getUTCDate();
            content += headerTable;
        }
        content += `
            <tr>
                <td>${s.quantity}</td>
                <td>${s.brand}</td>
                <td>${s.size}</td>
                <td>${s.color}</td>
                <td>${s.code}</td>
                <td>${s.price}</td>
                <td>${s.total}</td>
            </tr>`;
            articulos += s.quantity;
            ventas += s.total;
            ganancias += s.cost * s.quantity;
    });
    content += endTable;
    ganancias = ventas - ganancias;
    content+=`
        <br><br><div style="position: absolute;">
            <table class="table table-bordered" style="text-align: center;">
                <thead>
                    <tr>
                        <th colspan="2" scope="col">Resumen</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Articulos vendidos</b></td>
                        <td>${articulos}</td>
                    </tr>
                    <tr>
                        <td><b>Valor en ventas</b></td>
                        <td>$${parseFloat(ventas).toFixed(2)}</td>
                    </tr>
                    <tr>
                        <td><b>Valor de ganancias</b></td>
                        <td>$${parseFloat(ganancias).toFixed(2)}</td>
                    </tr>
                </tbody>
            </table>
        </div>`;

    
    return content;
}

module.exports = { 
    "generateDailyReport": generateDailyReport,
    "generateMonthlyReport": generateMonthlyReport,
    "loadCharts": loadCharts
}
