const filterForm = document.querySelector('#filterForm')
const filterGeneral = document.querySelector('#filterGeneral')
const filterProvider = document.querySelector('#filterProvider')
const filterBrand = document.querySelector('#filterBrand')
const filterCode = document.querySelector('#filterCode')
const filterColor = document.querySelector('#filterColor')
const filterSize = document.querySelector('#filterSize')
const buttonAdvancedSearch = document.querySelector('#advancedSearchButton')

function filterWithGeneralSearch(products) {
    let filteredProducts = products;
    if(filterGeneral.value!==''){
        let filter = filterGeneral.value.toLowerCase();
        filteredProducts = products.filter(function (p) {
            return  p.provider.toLowerCase().includes(filter) || 
                    p.brand.toLowerCase().includes(filter) ||
                    p.code.toLowerCase().includes(filter) ||
                    p.color.toLowerCase().includes(filter) ||
                    p.size.includes(filter);
        });
        return filteredProducts;
    } else {
        return products;
    }
}

function filterWithAdvancedSearch(products) {
    let filteredProducts = products;
    let provider = filterProvider.value;
    let brand = filterBrand.value;
    let code = filterCode.value;
    let color = filterColor.value;
    let size = filterSize.value;
    if(provider!=='' || brand!=='' || code!=='' || color!=='' || size!==''){
        if(provider!=='') {
            filteredProducts = filteredProducts.filter(function (p) {
                return  p.provider.toLowerCase().includes(provider.toLowerCase());
            });
        }
            
        if(brand!==''){
            filteredProducts = filteredProducts.filter(function (p) {
                return  p.brand.toLowerCase().includes(brand.toLowerCase());
            });
        }
            
        if(code!==''){
            filteredProducts = filteredProducts.filter(function (p) {
                return  p.code.toLowerCase().includes(code.toLowerCase());
            });
        }
            
        if(color!==''){
            filteredProducts = filteredProducts.filter(function (p) {
                return  p.color.toLowerCase().includes(color.toLowerCase());
            });
        }
            
        if(size!==''){
            filteredProducts = filteredProducts.filter(function (p) {
                return  p.size.includes(size);
            });
        }

        return filteredProducts;
    } else {
        return products;
    }
}

function startAdvancedSearch() {
    if(buttonAdvancedSearch.getAttribute('aria-expanded')==='true') {
        filterForm.reset();
        filterGeneral.disabled = false;
        return true;
        
    } else {
        filterGeneral.disabled = true;
        if(filterGeneral.value!=='') {
            filterGeneral.value = '';
            return true;
        }
        return false;
    }
}

module.exports = {
    "filterWithGeneralSearch": filterWithGeneralSearch,
    "filterWithAdvancedSearch": filterWithAdvancedSearch,
    "startAdvancedSearch": startAdvancedSearch
}
