const productList = document.querySelector('#productList')
const saleList = document.querySelector('#saleList')
const productToSellList = document.querySelector('#productToSellList')

function renderProductList(products) {
    productList.innerHTML = '';
    products.map(p => {
        productList.innerHTML += `
            <tr class="animate__animated animate__fadeIn">
                <td>${p.provider}</td>
                <td>${p.brand}</td>
                <td>${p.code}</td>
                <td>${p.color}</td>
                <td>${p.quantity}</td>
                <td>${p.size}</td>
                <td>${p.cost}</td>
                <td>${p.price}</td>
                <td>
                    <button onclick="deleteProduct('${p._id}')"
                        class="btn btn-danger"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Eliminar producto">
                            <i class="fa fa-trash"></i>
                    </button>
                    <button onclick="startEditProduct('${p._id}')"
                        class="btn btn-warning"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Editar producto">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button onclick="copyProduct('${p._id}')"
                        class="btn btn-light"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Copiar datos">
                        <i class="fa fa-copy"></i>
                    </button>
                    <button onclick="addToSellList('${p._id}', '${p.provider}', '${p.brand}', '${p.code}', '${p.color}', '${p.size}', '${p.quantity}', '${p.price}', '${p.cost}')"
                        class="btn btn-primary"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Agregar al carrito">
                        <i class="fa fa-cart-plus"></i>
                    </button>
                </td>
            </tr>`;
    })
}

function renderSalesList(sales) {
    saleList.innerHTML = '';
    sales.reverse();
    sales.map(s => {
        saleList.innerHTML += `
            <tr class="animate__animated animate__fadeIn">
                <td>${s.date}</td>
                <td>${s.quantity}</td>
                <td>${s.brand}</td>
                <td>${s.size}</td>
                <td>${s.color}</td>
                <td>${s.code}</td>
                <td>${s.price}</td>
                <td>${s.total}</td>
            </tr>`;
    });
}

function renderProductToSellList(productsToSell) {
    productToSellList.innerHTML = '';
    productsToSell.map(p => {
        productToSellList.innerHTML += `
            <tr class="animate__animated animate__fadeIn">
                <td>${p.quantity}</td>
                <td>${p.brand}</td>
                <td>${p.size}</td>
                <td>${p.color}</td>
                <td>${p.code}</td>
                <td style="width: 15%">
                <input type="number"
                    id="price_${p.product_id}"
                    min="0.10"
                    step="0.01"
                    class="form-control"
                    value="${p.price}"
                    onblur="discountValue('${p.product_id}')">
                </td>
                <td>${p.total}</td>
                <td>
                    <button type="button" onclick="removeToSellList('${p.product_id}')" class="btn btn-primary">
                        <i class="fa fa-cart-arrow-down"></i>
                    </button>
                </td>
            </tr>`;
    });
}

module.exports = {
    "renderProductList": renderProductList,
    "renderSalesList": renderSalesList,
    "renderProductToSellList": renderProductToSellList
}