const productForm = document.querySelector('#productForm')
const productProvider = document.querySelector('#productProvider')
const productBrand = document.querySelector('#productBrand')
const productCode = document.querySelector('#productCode')
const productColor = document.querySelector('#productColor')
const productQuantity = document.querySelector('#productQuantity')
const productSize = document.querySelector('#productSize')
const productCost = document.querySelector('#productCost')
const productPrice = document.querySelector('#productPrice')
const submitButton = document.querySelector('#submitButton')

function createProductObject() {
    return product = {
        provider: productProvider.value,
        brand: productBrand.value,
        code: productCode.value,
        color: productColor.value,
        quantity: productQuantity.value,
        size: productSize.value,
        cost: productCost.value,
        price: productPrice.value
    }
}

function startEditProduct(products, id) {
    const product = products.find(p => p._id === id);

    submitButton.innerHTML = 'Editar';
    submitButton.classList.remove('btn-success');
    submitButton.classList.add('btn-warning');

    productProvider.value = product.provider;
    productBrand.value = product.brand;
    productCode.value = product.code;
    productColor.value = product.color;
    productQuantity.value = product.quantity;
    productSize.value = product.size;
    productCost.value = product.cost;
    productPrice.value = product.price;

    $("#collapseRegister").collapse('show');
}

function copyProduct(products, id) {
    const product = products.find(p => p._id === id);

    productProvider.value = product.provider;
    productBrand.value = product.brand;
    productCode.value = product.code;
    productColor.value = product.color;
    productQuantity.value = product.quantity;
    productSize.value = product.size;
    productCost.value = product.cost;
    productPrice.value = product.price;

    $("#collapseRegister").collapse('show');
}

function cleanForm() {
    productForm.reset();

    submitButton.innerHTML = 'Guardar';
    submitButton.classList.remove('btn-warning');
    submitButton.classList.add('btn-success');

    $("#collapseRegister").collapse('hide');
}

module.exports = {
    "createProductObject": createProductObject,
    "copyProduct": copyProduct,
    "cleanForm": cleanForm,
    "startEditProduct": startEditProduct
}