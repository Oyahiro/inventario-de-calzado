const productForm = document.querySelector('#productForm')
const submitButton = document.querySelector('#submitButton')

const saleForm = document.querySelector('#saleForm')

const notifier = require("node-notifier")
const { ipcRenderer } = require('electron')
const renderService = require('../services/renderService')
const searchService = require('../services/searchService')
const productService = require('../services/productService')
const saleService = require('../services/saleService')
const reportService = require('../services/reportService');

const __dirname = 'E:/Users/Alienware M15/git/inventario-de-calzado/resources/icons/';
let products = [];
let sales = [];
let productsToSell = [];
let updateStatus = false;
let idProductToUpdate = '';

// Start system
ipcRenderer.send('get-products');
ipcRenderer.send('get-sales');
// ipcRenderer.send('get-data-chart');
presentNotification('Lista de productos en linea');
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
  });

// ***************************************** PRODUCT SECTION *****************************************
productForm.addEventListener('submit', e => {
    e.preventDefault();
    const product = productService.createProductObject();
    if(!updateStatus) {
        ipcRenderer.send('create-product', product);
    } else {
        ipcRenderer.send('update-product', {...product, idProductToUpdate})
    }
    productForm.reset();
    $("#collapseRegister").collapse('hide');
}); 

function deleteProduct(id) {
    const result = confirm('Estas seguro de que quieres eliminar este dato');
    if(result) {
        ipcRenderer.send('delete-product', id);
    }
    return;
}

function startEditProduct(id) {
    updateStatus = true;
    idProductToUpdate = id;
    productService.startEditProduct(products, id);
}

function copyProduct(id) {
    productService.copyProduct(products, id);
}

function cleanProductForm() {
    updateStatus = false;
    idProductToUpdate = '';
    productService.cleanForm();
}

// ***************************************** FILTER SECTION *****************************************
function filterWithGeneralSearch() {
    let productsToRender = searchService.filterWithGeneralSearch(products);
    renderService.renderProductList(productsToRender);
}

function startAdvancedSearch() {
    let renderList = searchService.startAdvancedSearch();
    if (renderList) {
        renderService.renderProductList(products);
    }
}

function filterWithAdvancedSearch() {
    let productsToRender = searchService.filterWithAdvancedSearch(products);
    renderService.renderProductList(productsToRender);
}

// ***************************************** SALE SECTION *****************************************
saleForm.addEventListener('submit', e => {
    e.preventDefault();
    productsToSell.map(s => {
        ipcRenderer.send('register-sale', s);
    });
    cleanAllSale();
});

function addToSellList(product_id, provider, brand, code, color, size, quantity, price, cost) {
    var sale = {
        provider: provider,
        brand: brand,
        code: code,
        color: color,
        quantity: 1,
        size: size,
        cost: cost,
        price: price,

        date: null,
        product_id: product_id,
        total: price
    }
    productsToSell = saleService.addToSellList(sale, productsToSell, quantity);
    renderService.renderProductToSellList(productsToSell);
}

function removeToSellList(_id) {
    productsToSell = saleService.removeToSellList(_id, productsToSell);
    renderService.renderProductToSellList(productsToSell);
}

function discountValue(_id) {
    let new_val = 0;
    $(document).ready(function() {
        new_val = $("#price_" + _id).val();
        productsToSell = saleService.editToSellList(_id, new_val, productsToSell);
        renderService.renderProductToSellList(productsToSell);
    });
}

function cleanAllSale() {
    productsToSell = saleService.cleanAllSale();
    renderService.renderProductToSellList(productsToSell);
}

function addToBadge() {
    saleService.addToBadge();
}

function cleanBadgeNotifications() {
    saleService.cleanNotifications();
}

// ***************************************** REPORT SECTION *****************************************
function generateReport(type) {
    let contentPage = undefined;
    switch(type) {
        case 'daily':
            contentPage = reportService.generateDailyReport(sales, 'today');
            break;

        case 'daily_preview':
            contentPage = reportService.generateDailyReport(sales, 'preview');
            break;

        case 'monthly':
            contentPage = reportService.generateMonthlyReport(sales, 'actual');
            break;
    
        case 'monthly_preview':
            contentPage = reportService.generateMonthlyReport(sales, 'preview');
            break;

        case 'annual':
            console.log('Anual');
            break;
    }

    if(contentPage!=undefined)
        ipcRenderer.send("printPDF", contentPage);
}

function refreshChart() {
    reportService.loadCharts(sales);
}

// ***************************************** EVENTS SECTION *****************************************
ipcRenderer.on('products-obtained', (e, args) => {
    const productsReceived = JSON.parse(args);
    products = productsReceived;
    renderService.renderProductList(products);
});

ipcRenderer.on('product-created', (e, args) => {
    const newProduct = JSON.parse(args);
    products.push(newProduct);
    renderService.renderProductList(products);
    presentNotification('Producto agregado existosaente');
});

ipcRenderer.on('product-deleted', (e, args) => {
    const deletedProduct = JSON.parse(args);
    const newProducts = products.filter(p => {
        return p._id !== deletedProduct._id;
    });
    products = newProducts;
    presentNotification('Producto eliminado existosaente');
    renderService.renderProductList(products);
});

ipcRenderer.on('product-updated', (e, args) => {
    const updatedProduct = JSON.parse(args);
    products = products.map(p => {
        if(p._id === updatedProduct._id) {
            p.provider = updatedProduct.provider;
            p.brand = updatedProduct.brand;
            p.code = updatedProduct.code;
            p.color = updatedProduct.color;
            p.quantity = updatedProduct.quantity;
            p.size = updatedProduct.size;
            p.cost = updatedProduct.cost;
            p.price = updatedProduct.price;
        }
        return p;
    });
    // presentNotification('Producto actualizado existosaente');
    renderService.renderProductList(products);
    idProductToUpdate = '';
    updateStatus = false;

    submitButton.innerHTML = 'Guardar';
    submitButton.classList.remove('btn-warning');
    submitButton.classList.add('btn-success');
});

ipcRenderer.on('sales-obtained', (e, args) => {
    const salesReceived = JSON.parse(args);
    sales = salesReceived;
    sales = sales.map(s => {
        let ndate = new Date(s.date);
        s.date = ndate.toDateString();
        return s;
    })
    renderService.renderSalesList(sales);
});

ipcRenderer.on('sale-created', (e, args) => {
    let saleSaved =  JSON.parse(args);
    sales.reverse();
    idProductToUpdate = saleSaved.product_id;
    const product = products.find(p => p._id === saleSaved.product_id);
    product.quantity -= saleSaved.quantity;
    ipcRenderer.send('update-product', {...product, idProductToUpdate});
    saleSaved.date = new Date(saleSaved.date).toDateString();
    sales.push(saleSaved);
    renderService.renderSalesList(sales);
    presentNotification('Venta registrada exitosamente');
});

// ***************************************** NOTIFICATION *****************************************
function presentNotification(body) {
    notifier.notify({
        title: 'Sistema de inventario',
        message: body,
        sound: false
        // icon : __dirname + "information.png",
    }, function(err, response) {
        // console.log(err, response);
    });
} 